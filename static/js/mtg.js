var base_url = 'https://okke.kapsi.fi/card-images/normal/';
var lazyLoadInstance;

// Document loaded
document.addEventListener("DOMContentLoaded", function() {
    console.log('Init MTG!');

    // Enter submits search
    document.getElementById('searchText').onkeydown = function(e) {
        if (e.key == "Enter") {
            search();
        }
      };
      

    // Init lazy loading (https://github.com/verlok/lazyload)
    lazyLoadInstance = new LazyLoad({
        elements_selector: ".card"
        // ... more custom settings?
    });

});

// Clear DOM element childs
function clearElement(id) {
    var myNode = document.getElementById(id);
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

// Get values of checked checkboxes in array
function getSelected(names) {
    arr = [];
    results = document.querySelectorAll('[name="' + names + '"]:checked');
    results.forEach(function(item) {
        arr.push( item.value );
    });
    return arr;
}

// Stupid search
function search() {
    postData('/search/', {
        card: document.getElementById('searchText').value,
        color: getSelected('color'),
        rarity: getSelected('rarity'),
        type: document.getElementById('sel-type').value,
        search: document.getElementById('sel-search').value,
        sort: document.getElementById('sel-sort').value,
    })
    .then(data => show(data))
    .catch(error => console.error(error));
}

function postData(url = '', data = {}) {
    // Default options are marked with *
      return fetch(url, {
          method: 'POST', // *GET, POST, PUT, DELETE, etc.
          mode: 'cors', // no-cors, cors, *same-origin
          cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
          credentials: 'same-origin', // include, *same-origin, omit
          headers: {
              'Content-Type': 'application/json',
              // 'Content-Type': 'application/x-www-form-urlencoded',
          },
          redirect: 'follow', // manual, *follow, error
          referrer: 'no-referrer', // no-referrer, *client
          body: JSON.stringify(data), // body data type must match "Content-Type" header
      })
      .then(response => response.json()); // parses JSON response into native Javascript objects 
  }

function show(data) {
    var list = document.createElement('div');

    clearElement('resultsContainer');

    if( data.length == 0 ) {
        // Add text "No results"
        newP = document.createElement("p");
        text = document.createTextNode('No results');
        newP.appendChild(text);
        document.getElementById('resultsContainer').appendChild(newP);
        return;
    }

    for (var i = 0; i < data.length; i++) {
        // Create the image:
        var img = document.createElement('img');
        img.setAttribute('data-src', base_url + data[i]);
        img.classList.add('card');

        // Add it to the div:
        list.appendChild(img);
    }

    // Add the image container
    document.getElementById('resultsContainer').appendChild(list);

    // Lazy load them
    if (lazyLoadInstance) {
        lazyLoadInstance.update();
    }
}