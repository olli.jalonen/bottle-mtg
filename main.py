from bottle import request, route, run, template, static_file, get, post, default_app
import os
import sqlite3
import json
from sqlite3 import Error

# TODO: Limit + paginate results! (limit 50 added)

DB_FILE = 'cards.db'
VALID_SEARCH = ['all', 'name','type','oracle_text']
VALID_SORT   = ['name', 'edhrec_rank', 'cmc', 'color_identity']

# Load DB file (read only)
try:
    conn = sqlite3.connect(DB_FILE, uri=True)
except Error as e:
    print(e)
    exit(1)

#def db_search_card(name="terror"):
def db_search_card(options):
    name = options['card']
    ctype = options['type']
    color = '%'.join( sort_colors( options['color'] ) )
    rarity = ''.join( options['rarity'] )

    # Where to search from?
    search = 'all'
    if options['search'] in VALID_SEARCH:
        search = options['search']

    # Sort order
    sort = 'edhrec_rank'
    if options['sort'] in VALID_SORT:
        sort = options['sort']
    
    cur = conn.cursor()
    if search == 'all':
        # Special case
        cur.execute("SELECT encoded_name \
                    FROM cards \
                    WHERE (name LIKE ? \
                        OR type LIKE ? \
                        OR oracle_text LIKE ?) \
                        AND rarity LIKE ? \
                        AND color_identity LIKE ? \
                        AND type LIKE ? \
                    ORDER BY " + sort + " ASC \
                    LIMIT 50", 
            ('%'+name+'%', '%'+name+'%', '%'+name+'%', '%'+rarity+'%','%'+color+'%',ctype+'%',))
    else:
        # Normal search
        cur.execute("SELECT encoded_name \
                    FROM cards \
                    WHERE " + search + " LIKE ? \
                        AND rarity LIKE ? \
                        AND color_identity LIKE ? \
                        AND type LIKE ? \
                    ORDER BY " + sort + " ASC \
                    LIMIT 50", 
            ('%'+name+'%', '%'+rarity+'%','%'+color+'%',ctype+'%',))
 
    return cur.fetchall()

@route('/hello/<name>')
def hello(name):
    return template('<p>Hello {{name}}!</p>', name=name)

@get('/ssearch/<card>')
def search(card):
    cards = db_search_card(card)
    if cards == '':
        return template('<p>No cards found with {{card}} </p>', card=card)
    return template('<p>Found: {{text}}!</p>', text=cards)

# Colors are listed on this order
def sort_colors(colors=[]):
    order = {
        'B': 0,
        'G': 1,
        'R': 2,
        'U': 3,
        'W': 4
    }
    
    return sorted(colors, key=lambda x:order[x[0]])

@post('/search/')
def post_search():
    options = {
        "card": '',
        "color": [],
        "rarity": [],
        "type": '',
        "search": '',
        "sort": '',
    }

    for option in options:
        if option in request.json:
            options[ option ] = request.json[ option ]

    # print(options)
    if "card" in request.json:
        return json.dumps( db_search_card(options) )
    return json.dumps('[]')
    
@route('/static/<filename:path>')
def send_static(filename):
    this_directory = os.path.dirname(os.path.abspath(__file__))
    static_directory = os.path.join(this_directory, 'static/')
    return static_file(filename, root=static_directory)

@route('/')
def index():
    return template('base')


# these two lines are only used for python main.py
if __name__ == '__main__':
    run(host='0.0.0.0', port=80, debug=True, reloader=True)

# this is the hook for Gunicorn to run Bottle
main = default_app()
