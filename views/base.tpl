<html>
<head>
  <title>oMTG Search</title>
  <!-- UIkit CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/css/uikit.min.css" />

  <!-- UIkit JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.1.5/js/uikit-icons.min.js"></script>

  <script src="https://cdn.jsdelivr.net/npm/vanilla-lazyload@12.0.0/dist/lazyload.min.js"></script>
  <script src="/static/js/mtg.js"></script>
  <link href="/static/css/mtg.css" rel="stylesheet" />
</head>
<body>
  <div class="container uk-background-default uk-container uk-container-large">
    <h1 class="uk-heading-small uk-heading-divider" id="header">oMTG Search<a class="uk-icon-button uk-position-right search" uk-icon="settings" uk-toggle="target: .options"></a></h1>
    <div class="row options" aria-hidden="true" hidden="" >
      <h3>Options</h3>
      <div class="column">
        <fieldset id="fset-color">      
          <legend>Color</legend>
          <div>
            <input class="uk-checkbox" type="checkbox" id="colorBlack" name="color" value="B">
            <label for="colorBlack">Black</label>
          </div>
          <div>
            <input class="uk-checkbox" type="checkbox" id="colorBlue" name="color" value="U">
            <label for="colorBlue">Blue</label>
          </div>
          <div>
            <input class="uk-checkbox" type="checkbox" id="colorGreen" name="color" value="G">
            <label for="colorGreen">Green</label>
          </div>
          <div>
            <input class="uk-checkbox" type="checkbox" id="colorRed" name="color" value="R">
            <label for="colorRed">Red</label>
          </div>
          <div>
            <input class="uk-checkbox" type="checkbox" id="colorWhite" name="color" value="W">
            <label for="colorWhite">White</label>
          </div>
        </fieldset>
      </div>

      <div class="column">
        <fieldset id="fset-rarity">      
          <legend>Rarity</legend>
          <div>
            <input class="uk-radio" type="radio" id="rarityAny" name="rarity" value="">
            <label for="rarityAny">Any</label>
          </div>
          <div>
            <input class="uk-radio" type="radio" id="rarityCommon" name="rarity" value="common">
            <label for="rarityCommon">Common</label>
          </div>
          <div>
            <input class="uk-radio" type="radio" id="rarityUncommon" name="rarity" value="uncommon">
            <label for="rarityUncommon">Uncommon</label>
          </div>
          <div>
            <input class="uk-radio" type="radio" id="rarityRare" name="rarity" value="rare">
            <label for="rarityRare">Rare</label>
          </div>
          <div>
            <input class="uk-radio" type="radio" id="rarityMythic" name="rarity" value="mythic">
            <label for="rarityMythic">Mythic</label>
          </div>
        </fieldset>
      </div>

      <div class="column">
        <div id="div-type">
          <label for="sel-type">Type:</label>
          <select class="uk-select" id="sel-type">
              <option value="" selected>Any</option>
              <option value="Artifact">Artifact</option>
              <option value="Artifact Creature">Artifact Creature</option>
              <option value="Creature">Creature</option>
              <option value="Enchantment">Enchantment</option>
              <option value="Instant">Instant</option>
              <option value="Land">Land</option>
              <option value="Legendary">Legendary</option>
              <option value="Legendary Creature">Legendary Creature</option>
              <option value="%Planeswalker">Planeswalker</option>
              <option value="Sorcery">Sorcery</option>
              <option value="Token">Token</option>
          </select>

        </div>

        <br/>
        
        <div id="div-search">
          <label for="sel-search">Search:</label>
          <select class="uk-select" id="sel-search">
              <option value="all" selected>Anywhere</option>
              <option value="name">Name</option>
              <option value="type">Type</option>
              <option value="oracle_text">Oracle</option>
          </select>
        </div>

        <br/>

        <div id="div-sort">
          <label for="sel-sort">Sort by:</label>
          <select class="uk-select" id="sel-sort">
              <option value="edhrec_rank" selected>EDHRec Rank</option>
              <option value="name">Name</option>
              <option value="cmc">Mana Cost</option>
              <option value="color_identity">Color</option>
          </select>
        </div>

      </div>
      
      <hr class="uk-divider-icon" />

    </div>

    <br/>

    <div class="uk-grid">
      <div class="uk-width-expand">
        <input class="uk-input  uk-text-large" type="text" size="50" id="searchText" />
      </div>
      <div class="uk-width-auto">
        <input class="uk-button uk-button-default  uk-text-large" type="button" id="searchBtn" value="Search" onClick="search()" />
      </div>
    </div>

    <hr class="uk-divider-icon" />

    <div id="resultsContainer"></div>
    <div class="center">
      <a class="uk-button uk-button-primary center" href="#header" uk-scroll>Scroll top</a>
    </div>

    <hr class="uk-divider-icon" />

    <div class="uk-container uk-container-small footer center">
      <p>oMTG Search is unofficial Fan Content permitted under the Fan Content Policy. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.</p>
      <p>Code is available @ https://gitlab.com/olli.jalonen/bottle-mtg/</p>
    </div>
  </div>
  
</body>
</html>